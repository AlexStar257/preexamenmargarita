function cargarAjax() {
    //direccion de 
    const url = "https://jsonplaceholder.typicode.com/todos"

    
        axios
        .get(url)//argumento ur de json
        .then((res) => {  //promesa : funcion que recibe otra funcion
            mostrar(res.data)
        })
        .catch((err) =>{
            console.log("surgio un error");
        })




function mostrar(data) { //IMPRIME LOS DATOS
    const res = document.getElementById('respuesta');
    res.innerHTML = "";
    for (let item of data) { //DATOS DE LA PAGINA
        res.innerHTML+=  `<tr>
        <td>${item.userId}</td>
        <td>${item.id}</td>
        <td>${item.title}</td>
        <td>${item.completed}</td>
        </tr>`;
    }
} 
}

//    BOTONES 
const res = document.getElementById("btnCargar");
res.addEventListener('click', function () {
    cargarAjax();
})

document.getElementById('btnLimpiar').addEventListener('click', function () {
    const res = document.getElementById('respuesta');
    res.innerHTML = "";
});
